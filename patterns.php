<?php
	require_once ('patternfunctions.php');


	// Local Pattern 1
	function LocalPattern1 ($LocalGames, $red_move, $green_move, $list) {
		// Parte Superior
		$pat = 'LP1';
		if ($red_move == 1 || $red_move == 2 || $red_move == 3 || $red_move == 4 || $red_move == 8 || $red_move == 9 || $red_move == 10 || $red_move == 11 || $red_move == 15 || $red_move == 16 || $red_move == 17 || $red_move == 22 || $red_move == 23 || $red_move == 24) {
			$green_move = 12;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = '5,6';
			$_SESSION['LocalGames']['LP2'][] = '18,19';
			$_SESSION['LocalGames']['LP3'][] = '7,13,14,20,21,26,27,31,32,33,34,35,37,38,39,40,41,42,43,44,45,46,47,48,49';
		}
		elseif ($red_move == 14 | $red_move == 20 | $red_move == 21) {
			$green_move = 38;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = '31,32';
			$_SESSION['LocalGames']['LP2'][] = '44,45';
			$_SESSION['LocalGames']['LP3'][] = '43,37,36,30,29,24,23,19,18,17,16,15,13,12,11,10,9,8,7,6,5,4,3,2,1';
		}
		elseif ($red_move == 13 | $red_move == 7) {
			$green_move = 19;
			unset_pattern ($pat, $green_move, $list);
//			$_SESSION['LocalGames']['LP4'][] = '24,18,17,16,12,11,10,9,7,6,5,4,3,2'; ESTA ASI EN EL PAPER
			$_SESSION['LocalGames']['LP4'][] = '24,18,17,16,12,11,10,9,6,5,4,3,2';
			$_SESSION['LocalGames']['LP5'][] = '20,26,27,31,32,33,34,37,38,39,40,41,43,44,45,46,47,48';
		}
		elseif ($red_move == 12) {
			$green_move = 19;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP6'][] = '24,18,17,16,13,11,10,9,7,6,5,4,3,2';
			$_SESSION['LocalGames']['LP5'][] = '20,26,27,31,32,33,34,37,38,39,40,41,43,44,45,46,47,48';
		}
		elseif ($red_move == 6) {
			$green_move = 20;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = '19,26';
			$_SESSION['LocalGames']['LP7'][] = '24,18,17,16,14,13,11,10,9,7,6,5,4,3,2';
			$_SESSION['LocalGames']['LP8'][] = '27,31,32,33,34,37,38,39,40,41,43,44,45,46,47,48';
		}
		elseif ($red_move == 5) {
			$green_move = 13;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = '6,7';
			$_SESSION['LocalGames']['LP9'][] = '19,18,17,12,11,10,4,3';
			$_SESSION['LocalGames']['LP10'][] = '20,26,27,31,32,33,34,37,38,39,40,41,42,43,44,45,46,47,48';
		}
		elseif ($red_move == 18) {
			$green_move = 19;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP11'][] = '3,4,5,6,7,10,11,12,13,14,16,17,20,23,24,26,27,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49';
		}
		elseif ($red_move == 19) {
			$green_move = 18;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP12'][] = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,20,21,22,23,24,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49';
		}
		// Parte Inferior
		elseif ($red_move == 49 || $red_move == 48 || $red_move == 47 || $red_move == 46 || $red_move == 42 || $red_move == 41 || $red_move == 40 || $red_move == 39 || $red_move == 35 || $red_move == 34 || $red_move == 33 || $red_move == 28 || $red_move == 27 || $red_move == 26) {
			$green_move = 38;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = '45,44';
			$_SESSION['LocalGames']['LP2'][] = '31,32';
			$_SESSION['LocalGames']['LP3'][] = '43,37,36,30,29,24,23,19,18,17,16,15,13,12,11,10,9,8,7,6,5,4,3,2,1';
		}
		elseif ($red_move == 36 | $red_move == 30 | $red_move == 29) {
			$green_move = 12;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = '19,18';
			$_SESSION['LocalGames']['LP2'][] = '6,5';
			$_SESSION['LocalGames']['LP3'][] = '7,13,14,20,21,26,27,31,32,33,34,35,37,38,39,40,41,42,43,44,45,46,47,48,49';
		}
		elseif ($red_move == 37 | $red_move == 43) {
			$green_move = 31;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP4'][] = '26,32,33,34,38,39,40,41,43,44,45,46,47,48';
			$_SESSION['LocalGames']['LP5'][] = '30,24,23,19,18,17,16,13,12,11,10,9,7,6,5,4,3,2';
		}
		elseif ($red_move == 38) {
			$green_move = 31;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP6'][] = '26,32,33,34,37,39,40,41,43,44,45,46,47,48';
			$_SESSION['LocalGames']['LP5'][] = '30,24,23,19,18,17,16,13,12,11,10,9,7,6,5,4,3,2';
		}
		elseif ($red_move == 44) {
			$green_move = 30;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = '31,24';
			$_SESSION['LocalGames']['LP7'][] = '26,32,33,34,36,37,39,40,41,43,44,45,46,47,48';
			$_SESSION['LocalGames']['LP8'][] = '23,19,18,17,13,12,11,10,9,7,6,5,4,3,2';
		}
		elseif ($red_move == 45) {
			$green_move = 37;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = '44,43';
			$_SESSION['LocalGames']['LP9'][] = '31,32,33,38,39,40,46,47';
			$_SESSION['LocalGames']['LP10'][] = '30,24,23,19,18,17,16,13,12,11,10,9,8,7,6,5,4,3,2';
		}
		elseif ($red_move == 32) {
			$green_move = 31;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP11'][] = '47,46,45,44,43,40,39,38,37,36,34,33,30,27,26,24,23,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1';
		}
		elseif ($red_move == 31) {
			$green_move = 32;
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP12'][] = '49,48,47,45,44,43,42,41,40,39,38,37,36,35,34,33,30,29,28,27,26,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1';
		}
		return $green_move;
	}

	// Local Pattern 2
	function LocalPattern2 ($LocalGames, $red_move, $green_move, $list) {
		$pat = 'LP2';
		$pos = explode (',', $list);
		$clave = 0; 
		foreach ($LocalGames[$pat] as $key => $value) {
			if ($value == $list) {
				$clave = $key;
			}
		}
		if ($red_move == $pos[0]) {
			$green_move = $pos[1];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $pos[1]) {
			$green_move = $pos[0];
			unset_pattern ($pat, $green_move, $list);
		}
		return $green_move;
	}


	// Local Pattern 3
	function LocalPattern3 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP3';
		if ($red_move == $x[0] || $red_move == $x[1] || $red_move == $x[2] || $red_move == $x[3] || $red_move == $x[4] || $red_move == $x[5] || $red_move == $x[6] || $red_move == $x[9] || $red_move == $x[10] || $red_move == $x[11] || $red_move == $x[12] || $red_move == $x[14] || $red_move == $x[15] || $red_move == $x[16] || $red_move == $x[17] || $red_move == $x[18] || $red_move == $x[21] || $red_move == $x[22] || $red_move == $x[23] || $red_move == $x[24]) {
			$green_move = $x[13];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[7].','.$x[8];
			$_SESSION['LocalGames']['LP2'][] = $x[19].','.$x[20];
		}
		elseif ($red_move == $x[7] || $red_move == $x[13] || $red_move == $x[19] ) {
			$green_move = $x[9];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[5].','.$x[8];
			$_SESSION['LocalGames']['LP13'][] = $x[10].','.$x[14].','.$x[15].','.$x[16].','.$x[20].','.$x[21].','.$x[22].','.$x[23];
		}
		elseif ($red_move == $x[20]) {
			$green_move = $x[12];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[18].','.$x[19];
			$_SESSION['LocalGames']['LP9'][] = $x[7].','.$x[8].','.$x[9].','.$x[13].','.$x[14].','.$x[15].','.$x[21].','.$x[22];
		}
		elseif ($red_move == $x[8]) {
			$green_move = $x[7];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP14'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[5].','.$x[6].','.$x[9].','.$x[10].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22];
		}
		return $green_move;
	}

	// Local Pattern 4
	function LocalPattern4 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP4';
		if ($red_move == $x[0] || $red_move == $x[1] || $red_move == $x[2] || $red_move == $x[3] || $red_move == $x[5] || $red_move == $x[6] || $red_move == $x[7] || $red_move == $x[10] || $red_move == $x[11] || $red_move == $x[12]) {
			$green_move = $x[4];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[9].','.$x[8];
		}
		elseif ($red_move == $x[4] | $red_move == $x[8]) {
			$green_move = $x[2];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[0].','.$x[1];
			$_SESSION['LocalGames']['LP13'][] = $x[3].','.$x[5].','.$x[6].','.$x[7].','.$x[9].','.$x[10].','.$x[11].','.$x[12];
		}
		elseif ($red_move == $x[9] ) {
			$green_move = $x[4];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP15'][] = $x[0].','.$x[1].','.$x[2].','.$x[5].','.$x[6].','.$x[8].','.$x[10].','.$x[11];
		}
		return $green_move;
	}

	// Local Pattern 5
	function LocalPattern5 ($LocalGames, $red_move, $green_move, $list) {
		$pat = 'LP5';
		$x = explode (',', $list);
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[7] | $red_move == $x[10] | $red_move == $x[11] | $red_move == $x[12] | $red_move == $x[13]) {
			$green_move = $x[4];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[8].','.$x[9];
		}
		elseif ($red_move == $x[9] | $red_move == $x[8] | $red_move == $x[4]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[0].','.$x[1];
			$_SESSION['LocalGames']['LP13'][] = $x[3].','.$x[5].','.$x[6].','.$x[7].','.$x[10].','.$x[11].','.$x[12].','.$x[13];
		}
		return $green_move;
	}

	// Local Pattern 6
	function LocalPattern6 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP6';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[6] | $red_move == $x[7] | $red_move == $x[8] | $red_move == $x[10] | $red_move == $x[11] | $red_move == $x[12] | $red_move == $x[13]) {
			$green_move = $x[9];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[4].','.$x[5];
		}
		elseif ($red_move == $x[9] | $red_move == $x[5] | $red_move == $x[4]) {
			$green_move = $x[2];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[0].','.$x[1];
			$_SESSION['LocalGames']['LP13'][] = $x[3].','.$x[6].','.$x[7].','.$x[8].','.$x[10].','.$x[11].','.$x[12].','.$x[13];
		}
		return $green_move;
	}

	// Local Pattern 7
	function LocalPattern7 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP7';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[9] | $red_move == $x[10] | $red_move == $x[11] | $red_move == $x[15] | $red_move == $x[16] | $red_move == $x[17]) {
			$green_move = $x[8];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[3].','.$x[4 ];
			$_SESSION['LocalGames']['LP2'][] = $x[13].','.$x[14];
		}
		elseif ($red_move == $x[3] | $red_move == $x[7] | $red_move == $x[8] | $red_move == $x[12] | $red_move == $x[13]) {
			$green_move = $x[5];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[1].','.$x[4];
			$_SESSION['LocalGames']['LP13'][] = $x[6].','.$x[9].','.$x[10].','.$x[11].','.$x[14].','.$x[15].','.$x[16].','.$x[17];
		}
		elseif ($red_move == $x[14]) {
			$green_move = $x[7];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[12].','.$x[13];
			$_SESSION['LocalGames']['LP9'][] = $x[3].','.$x[4].','.$x[5].','.$x[8].','.$x[9].','.$x[10].','.$x[15].','.$x[16];
		}
		elseif ($red_move == $x[4]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP16'][] = $x[0].','.$x[1].','.$x[2].','.$x[5].','.$x[6].','.$x[7].','.$x[8].','.$x[9].','.$x[10].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[16];
		}
		return $green_move;
}

	// Local Pattern 8
	function LocalPattern8 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP8';
		if ($red_move == $x[0] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[7] | $red_move == $x[8] | $red_move == $x[9] | $red_move == $x[10] | $red_move == $x[13] | $red_move == $x[14] | $red_move == $x[15]) {
			$green_move = $x[6];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[1].','.$x[2];
			$_SESSION['LocalGames']['LP2'][] = $x[11].','.$x[12];
		}
		elseif ($red_move == $x[1] | $red_move == $x[6] | $red_move == $x[11]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[0].','.$x[2];
			$_SESSION['LocalGames']['LP13'][] = $x[4].','.$x[7].','.$x[8].','.$x[9].','.$x[12].','.$x[13].','.$x[14].','.$x[15];
		}
		elseif ($red_move == $x[12]) {
			$green_move = $x[5];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[10].','.$x[11];
			$_SESSION['LocalGames']['LP9'][] = $x[1].','.$x[2].','.$x[3].','.$x[6].','.$x[7].','.$x[8].','.$x[13].','.$x[14];
		}
		elseif ($red_move == $x[2]) {
			$green_move = $x[1];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP17'][] = $x[0].','.$x[3].','.$x[4].','.$x[5].','.$x[6].','.$x[7].','.$x[8].','.$x[10].','.$x[11].','.$x[12].','.$x[13].','.$x[14];
		}
		return $green_move;
	}


	// Local Pattern 9
	function LocalPattern9 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP9';
		if ($red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[7]) {
			$green_move = $x[0];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[0]) {
			$green_move = $x[1];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP18'][] = $x[2].','.$x[3].','.$x[4].','.$x[5].','.$x[6].','.$x[7];
		}
		return $green_move;
	}


	// Local Pattern 10
	function LocalPattern10 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP10';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[7] | $red_move == $x[9] | $red_move == $x[10] | $red_move == $x[11] | $red_move == $x[12] | $red_move == $x[15] | $red_move == $x[16] | $red_move == $x[17]) {
			$green_move = $x[8];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[3].','.$x[4];
			$_SESSION['LocalGames']['LP2'][] = $x[13].','.$x[14];
		}
		elseif ($red_move == $x[3] | $red_move == $x[8] | $red_move == $x[13]) {
			$green_move = $x[5];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[1].','.$x[4];
			$_SESSION['LocalGames']['LP13'][] = $x[6].','.$x[9].','.$x[10].','.$x[11].','.$x[14].','.$x[15].','.$x[16].','.$x[17];
		}
		elseif ($red_move == $x[14]) {
			$green_move = $x[7];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[12].','.$x[13];
			$_SESSION['LocalGames']['LP9'][] = $x[3].','.$x[4].','.$x[5].','.$x[8].','.$x[9].','.$x[10].','.$x[15].','.$x[16];
		}
		elseif ($red_move == $x[4]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP19'][] = $x[0].','.$x[1].','.$x[2].','.$x[5].','.$x[6].','.$x[7].','.$x[8].','.$x[9].','.$x[10].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[16];
		}
		return $green_move;
	}

	// Local Pattern 11
	function LocalPattern11 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP11';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[9] | $red_move == $x[10] | $red_move == $x[11] | $red_move == $x[13] | $red_move == $x[14] | $red_move == $x[17] | $red_move == $x[18] | $red_move == $x[24]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[7].','.$x[8];
			//$_SESSION['LocalGames']['LP5'][] = $x[12].','.$x[15].','.$x[16].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[31].','.$x[32].','.$x[33].','.$x[34].','.$x[35].','.$x[36];
			$_SESSION['LocalGames']['LP7'][] = $x[12].','.$x[15].','.$x[16].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[31].','.$x[32].','.$x[33].','.$x[34].','.$x[35].','.$x[36];
		}
		elseif ($red_move == $x[7]) {
			$green_move = $x[8];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[3].','.$x[4];
			//$_SESSION['LocalGames']['LP5'][] = $x[12].','.$x[15].','.$x[16].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[31].','.$x[32].','.$x[33].','.$x[34].','.$x[35].','.$x[36];
			$_SESSION['LocalGames']['LP7'][] = $x[12].','.$x[15].','.$x[16].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[31].','.$x[32].','.$x[33].','.$x[34].','.$x[35].','.$x[36];
		}
		elseif ($red_move == $x[8]) {
			$green_move = $x[7];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[2].','.$x[3];
			//$_SESSION['LocalGames']['LP5'][] = $x[12].','.$x[15].','.$x[16].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[31].','.$x[32].','.$x[33].','.$x[34].','.$x[35].','.$x[36];
			$_SESSION['LocalGames']['LP7'][] = $x[12].','.$x[15].','.$x[16].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[31].','.$x[32].','.$x[33].','.$x[34].','.$x[35].','.$x[36];
		}
		elseif ($red_move == $x[12] | $red_move == $x[15] | $red_move == $x[16] | $red_move == $x[21] | $red_move == $x[22] | $red_move == $x[23] | $red_move == $x[27] | $red_move == $x[28] | $red_move == $x[29]  | $red_move == $x[30] | $red_move == $x[34] | $red_move == $x[35] | $red_move == $x[36] | $red_move == $x[37]) {
			$green_move = $x[26];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[19].','.$x[20];
			$_SESSION['LocalGames']['LP2'][] = $x[32].','.$x[33];
			$_SESSION['LocalGames']['LP14'][] = $x[31].','.$x[25].','.$x[24].','.$x[18].','.$x[17].','.$x[14].','.$x[13].','.$x[11].','.$x[10].','.$x[8].','.$x[7].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[25] | $red_move == $x[31]) {
			$green_move = $x[19];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP4'][] = $x[15].','.$x[20].','.$x[21].','.$x[22].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[32].','.$x[33].','.$x[34].','.$x[35].','.$x[36];
			$_SESSION['LocalGames']['LP16'][] = $x[18].','.$x[14].','.$x[13].','.$x[11].','.$x[10].','.$x[8].','.$x[7].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[32]) {
			$green_move = $x[18];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[14].','.$x[19];
			$_SESSION['LocalGames']['LP17'][] = $x[13].','.$x[11].','.$x[10].','.$x[8].','.$x[7].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
			//$_SESSION['LocalGames']['LP7'][] = $x[15].','.$x[20].','.$x[21].','.$x[22].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[31].','.$x[33].','.$x[34].','.$x[35].','.$x[36];
			$_SESSION['LocalGames']['LP5'][] = $x[15].','.$x[20].','.$x[21].','.$x[22].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[31].','.$x[33].','.$x[34].','.$x[35].','.$x[36];
		}
		elseif ($red_move == $x[33]) {
			$green_move = $x[25];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[31].','.$x[32];
			$_SESSION['LocalGames']['LP9'][] = $x[19].','.$x[20].','.$x[21].','.$x[26].','.$x[27].','.$x[28].','.$x[34].','.$x[35];
			$_SESSION['LocalGames']['LP17'][] = $x[13].','.$x[11].','.$x[10].','.$x[8].','.$x[7].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[26]) {
			$green_move = $x[19];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP6'][] = $x[15].','.$x[20].','.$x[21].','.$x[22].','.$x[25].','.$x[27].','.$x[28].','.$x[29].','.$x[31].','.$x[32].','.$x[33].','.$x[34].','.$x[35].','.$x[36];
			$_SESSION['LocalGames']['LP16'][] = $x[18].','.$x[14].','.$x[13].','.$x[11].','.$x[10].','.$x[8].','.$x[7].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[20]) {
			$green_move = $x[19];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP16'][] = $x[2].','.$x[15].','.$x[17].','.$x[21].','.$x[22].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[31].','.$x[32].','.$x[33].','.$x[34].','.$x[35];
			$_SESSION['LocalGames']['LP16'][] = $x[18].','.$x[14].','.$x[13].','.$x[11].','.$x[10].','.$x[8].','.$x[7].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[19]) {
			$green_move = $x[20];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP20'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[5].','.$x[6].','.$x[7].','.$x[8].','.$x[9].','.$x[10].','.$x[11].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[16].','.$x[17].','.$x[18].','.$x[21].','.$x[22].','.$x[23].','.$x[24].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[30].','.$x[31].','.$x[32].','.$x[33].','.$x[34].','.$x[35].','.$x[36].','.$x[37];
		}
		elseif ($red_move == $x[3]) {
			$green_move = $x[7];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP35'][] = $x[0].','.$x[1].','.$x[2].','.$x[5].','.$x[6].','.$x[10].','.$x[11].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[16].','.$x[17].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[23].','.$x[24].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[30].','.$x[31].','.$x[32].','.$x[33].','.$x[34].','.$x[35].','.$x[36].','.$x[37];
		}
		return $green_move;
	}


	// Local Pattern 12
	function LocalPattern12 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP12';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[6] | $red_move == $x[7] | $red_move == $x[8] | $red_move == $x[12] | $red_move == $x[13] | $red_move == $x[14] | $red_move == $x[15]  | $red_move == $x[17] | $red_move == $x[18] | $red_move == $x[19] | $red_move == $x[20] | $red_move == $x[21] | $red_move == $x[22] | $red_move == $x[23]  | $red_move == $x[24] | $red_move == $x[25] | $red_move == $x[26] | $red_move == $x[29] | $red_move == $x[30] | $red_move == $x[31] | $red_move == $x[32]  | $red_move == $x[33] | $red_move == $x[35] | $red_move == $x[36] | $red_move == $x[37] | $red_move == $x[38] | $red_move == $x[39] | $red_move == $x[42]  | $red_move == $x[43] | $red_move == $x[44] | $red_move == $x[45]) {
			$green_move = $x[34];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[27].','.$x[28];
			$_SESSION['LocalGames']['LP2'][] = $x[40].','.$x[41];
			$_SESSION['LocalGames']['LP13'][] = $x[16].','.$x[11].','.$x[10].','.$x[9].','.$x[5].','.$x[4].','.$x[3].','.$x[2];
		}
		elseif ($red_move == $x[27] | $red_move == $x[34] | $red_move == $x[40]) {
			$green_move = $x[29];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[22].','.$x[28];
			$_SESSION['LocalGames']['LP13'][] = $x[30].','.$x[35].','.$x[36].','.$x[37].','.$x[41].','.$x[42].','.$x[43].','.$x[44];
			$_SESSION['LocalGames']['LP13'][] = $x[16].','.$x[11].','.$x[10].','.$x[9].','.$x[5].','.$x[4].','.$x[3].','.$x[2];
		}
		elseif ($red_move == $x[2] | $red_move == $x[3] | $red_move == $x[9] | $red_move == $x[10] | $red_move == $x[16]) {
			$green_move = $x[11];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[4].','.$x[5];
			$_SESSION['LocalGames']['LP3'][] = $x[6].','.$x[12].','.$x[13].','.$x[17].','.$x[18].','.$x[22].','.$x[23].','.$x[27].','.$x[28].','.$x[29].','.$x[30].','.$x[31].','.$x[33].','.$x[34].','.$x[35].','.$x[36].','.$x[37].','.$x[38].','.$x[39].','.$x[40].','.$x[41].','.$x[42].','.$x[43].','.$x[44].','.$x[45];
		}
		elseif ($red_move == $x[41]) {
			$green_move = $x[33];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[39].','.$x[40];
			$_SESSION['LocalGames']['LP9'][] = $x[27].','.$x[28].','.$x[29].','.$x[34].','.$x[35].','.$x[36].','.$x[42].','.$x[43];
			$_SESSION['LocalGames']['LP13'][] = $x[16].','.$x[11].','.$x[10].','.$x[9].','.$x[5].','.$x[4].','.$x[3].','.$x[2];
		}
		elseif ($red_move == $x[28]) {
			$green_move = $x[27];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP20'][] = $x[43].','.$x[42].','.$x[41].','.$x[40].','.$x[39].','.$x[36].','.$x[35].','.$x[34].','.$x[33].','.$x[32].','.$x[30].','.$x[29].','.$x[26].','.$x[23].','.$x[22].','.$x[21].','.$x[20].','.$x[18].','.$x[17].','.$x[16].','.$x[15].','.$x[14].','.$x[13].','.$x[12].','.$x[11].','.$x[10].','.$x[9].','.$x[8].','.$x[7].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[5] | $red_move == $x[11]) {
			$green_move = $x[27];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP13'][] = $x[28].','.$x[33].','.$x[34].','.$x[35].','.$x[39].','.$x[40].','.$x[41].','.$x[42];
			$_SESSION['LocalGames']['LP22'][] = $x[26].','.$x[21].','.$x[20].','.$x[16].','.$x[15].','.$x[14].','.$x[10].','.$x[9].','.$x[8].','.$x[7].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[4]) {
			$green_move = $x[11];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP23'][] = $x[16].','.$x[10].','.$x[9].','.$x[5].','.$x[3].','.$x[2];
			$_SESSION['LocalGames']['LP3'][] = $x[6].','.$x[12].','.$x[13].','.$x[17].','.$x[18].','.$x[22].','.$x[23].','.$x[27].','.$x[28].','.$x[29].','.$x[30].','.$x[31].','.$x[33].','.$x[34].','.$x[35].','.$x[36].','.$x[37].','.$x[38].','.$x[39].','.$x[40].','.$x[41].','.$x[42].','.$x[43].','.$x[44].','.$x[45].','.$x[46];
		}
		return $green_move;
	}

	// Local Pattern 13
	function LocalPattern13 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP13';
		if ($red_move == $x[0] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[6] | $red_move == $x[7]) {
			$green_move = $x[1];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[4].','.$x[5];
		}
		elseif ($red_move == $x[1] | $red_move == $x[4] | $red_move == $x[5]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[0].','.$x[2];
			$_SESSION['LocalGames']['LP2'][] = $x[6].','.$x[7];
		}
		return $green_move;
	}




	// Local Pattern 14
	function LocalPattern14 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP14';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[7] | $red_move == $x[8] | $red_move == $x[11] | $red_move == $x[12] | $red_move == $x[13] | $red_move == $x[15] | $red_move == $x[16] | $red_move == $x[17]) {
			$green_move = $x[14];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[9].','.$x[10];
		}
		elseif ($red_move == $x[9]) {
			$green_move = $x[10];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[14].','.$x[15];
		}
		elseif ($red_move == $x[10]) {
			$green_move = $x[9];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[13].','.$x[14];
		}
		elseif ($red_move == $x[14]) {
			$green_move = $x[10];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[5].','.$x[6].','.$x[7].','.$x[8].','.$x[11].','.$x[12].','.$x[15].','.$x[16].','.$x[17];
		}
		return $green_move;
	}


	// Local Pattern 15
	function LocalPattern15 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP15';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[6] | $red_move == $x[7]) {
			$green_move = $x[5];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[5]) {
			$green_move = $x[4];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[6].','.$x[7];
			$_SESSION['LocalGames']['LP25'][] = $x[0].','.$x[1].','.$x[2].','.$x[3];
		}
		return $green_move;
	}

	// Local Pattern 16
	function LocalPattern16 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP16';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[7] | $red_move == $x[8] | $red_move == $x[9] | $red_move == $x[11] | $red_move == $x[12] | $red_move == $x[13]) {
			$green_move = $x[10];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[5].','.$x[6];
		}
		elseif ($red_move == $x[5]) {
			$green_move = $x[7];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[10].','.$x[11];
		}
		elseif ($red_move == $x[6]) {
			$green_move = $x[7];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[9].','.$x[10];
		}
		elseif ($red_move == $x[10]) {
			$green_move = $x[6];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP26'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[7].','.$x[8].','.$x[11].','.$x[12].','.$x[13];
		}
		return $green_move;
	}

	// Local Pattern 17
	function LocalPattern17 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP17';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[7] | $red_move == $x[9] | $red_move == $x[10] | $red_move == $x[11]) {
			$green_move = $x[8];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[3].','.$x[4];
		}
		elseif ($red_move == $x[3]) {
			$green_move = $x[4];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[8].','.$x[9];
		}
		elseif ($red_move == $x[4]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[7].','.$x[8];
		}
		elseif ($red_move == $x[8]) {
			$green_move = $x[4];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP27'][] = $x[0].','.$x[1].','.$x[2].','.$x[5].','.$x[6].','.$x[9].','.$x[10].','.$x[11];
		}
		return $green_move;
	}

	// Local Pattern 18
	function LocalPattern18 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP18';
		if ($red_move == $x[0] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5]) {
			$green_move = $x[1];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[1]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[0].','.$x[2];
			$_SESSION['LocalGames']['LP2'][] = $x[4].','.$x[5];
		}
		return $green_move;
	}

	// Local Pattern 19
	function LocalPattern19 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP19';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[7] | $red_move == $x[8] | $red_move == $x[9] | $red_move == $x[11] | $red_move == $x[12] | $red_move == $x[13]) {
			$green_move = $x[10];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[5].','.$x[6];
		}
		elseif ($red_move == $x[5]) {
			$green_move = $x[6];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[10].','.$x[11];
		}
		elseif ($red_move == $x[6]) {
			$green_move = $x[5];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[9].','.$x[10];
		}
		elseif ($red_move == $x[10]) {
			$green_move = $x[6];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP28'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[7].','.$x[8].','.$x[11].','.$x[12].','.$x[13];
		}
		return $green_move;
	}

	// Local Pattern 20
	function LocalPattern20 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP20';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[9] | $red_move == $x[10] | $red_move == $x[11] | $red_move == $x[12] | $red_move == $x[13] | $red_move == $x[14] | $red_move == $x[15] | $red_move == $x[16] | $red_move == $x[17] | $red_move == $x[18] | $red_move == $x[20] | $red_move == $x[21] | $red_move == $x[22] | $red_move == $x[23] | $red_move == $x[27] | $red_move == $x[28] | $red_move == $x[29] | $red_move == $x[34] | $red_move == $x[35]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[7].','.$x[8];
			$_SESSION['LocalGames']['LP13'][] = $x[19].','.$x[24].','.$x[25].','.$x[26].','.$x[30].','.$x[31].','.$x[32].','.$x[33];
		}
		elseif ($red_move == $x[7]) {
			$green_move = $x[8];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[3].','.$x[4];
			$_SESSION['LocalGames']['LP13'][] = $x[19].','.$x[24].','.$x[25].','.$x[26].','.$x[30].','.$x[31].','.$x[32].','.$x[33];
		}
		elseif ($red_move == $x[8]) {
			$green_move = $x[7];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[2].','.$x[3];
			$_SESSION['LocalGames']['LP13'][] = $x[19].','.$x[24].','.$x[25].','.$x[26].','.$x[30].','.$x[31].','.$x[32].','.$x[33];
		}
		elseif ($red_move == $x[19] | $red_move == $x[25] | $red_move == $x[26] | $red_move == $x[32] | $red_move == $x[33]) {
			$green_move = $x[24];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[30].','.$x[31];
			$_SESSION['LocalGames']['LP14'][] = $x[29].','.$x[23].','.$x[22].','.$x[18].','.$x[17].','.$x[14].','.$x[13].','.$x[11].','.$x[10].','.$x[8].','.$x[7].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[24] | $red_move == $x[30]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[7].','.$x[8];
			$_SESSION['LocalGames']['LP22'][] = $x[12].','.$x[15].','.$x[16].','.$x[19].','.$x[20].','.$x[21].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[31].','.$x[32].','.$x[33].','.$x[34].','.$x[35];
		}
		elseif ($red_move == $x[31]) {
			$green_move = $x[24];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP23'][] = $x[19].','.$x[25].','.$x[26].','.$x[30].','.$x[32].','.$x[33];
			$_SESSION['LocalGames']['LP14'][] = $x[29].','.$x[23].','.$x[22].','.$x[18].','.$x[17].','.$x[14].','.$x[13].','.$x[11].','.$x[10].','.$x[8].','.$x[7].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[3]) {
			$green_move = $x[8];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP29'][] = $x[0].','.$x[1].','.$x[2].','.$x[5].','.$x[6].','.$x[10].','.$x[11].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[16].','.$x[17].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[23].','.$x[24].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[30].','.$x[31].','.$x[32].','.$x[34].','.$x[35];
		}
		return $green_move;
	}

	// Local Pattern 21
	function LocalPattern21 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP21';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[8] | $red_move == $x[9]) {
			$green_move = $x[7];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[7]) {
			$green_move = $x[6];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[8].','.$x[9];
			$_SESSION['LocalGames']['LP30'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[5];
		}
		return $green_move;
	}

	// Local Pattern 22
	function LocalPattern22 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP22';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[7] | $red_move == $x[8] | $red_move == $x[9] | $red_move == $x[12] | $red_move == $x[13] | $red_move == $x[14]) {
			$green_move = $x[6];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[10].','.$x[11];
		}
		elseif ($red_move == $x[6] | $red_move == $x[10]) {
			$green_move = $x[4];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP13'][] = $x[5].','.$x[7].','.$x[8].','.$x[9].','.$x[11].','.$x[12].','.$x[13].','.$x[14];
			$_SESSION['LocalGames']['LP25'][] = $x[0].','.$x[1].','.$x[2].','.$x[3];
		}
		elseif ($red_move == $x[11]) {
			$green_move = $x[6];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP21'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[7].','.$x[8].','.$x[10].','.$x[12].','.$x[13];
		}
		return $green_move;
	}

	// Local Pattern 23
	function LocalPattern23 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP23';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[4] | $red_move == $x[5]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[3]) {
			$green_move = $x[2];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[0].','.$x[1];
			$_SESSION['LocalGames']['LP2'][] = $x[4].','.$x[5];
		}
		return $green_move;
	}

	// Local Pattern 24
	function LocalPattern24 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP24';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[7] | $red_move == $x[8] | $red_move == $x[9] | $red_move == $x[10] | $red_move == $x[12] | $red_move == $x[13]) {
			$green_move = $x[11];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[11]) {
			$green_move = $x[10];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[12].','.$x[13];
			$_SESSION['LocalGames']['LP31'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[5].','.$x[6].','.$x[7].','.$x[8].','.$x[9];
		}
		return $green_move;
	}

	// Local Pattern 25
	function LocalPattern25 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP25';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[3]) {
			$green_move = $x[2];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[0].','.$x[1];
		}
		return $green_move;
	}

	// Local Pattern 26
	function LocalPattern26 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP26';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[8] | $red_move == $x[9]) {
			$green_move = $x[7];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[7]) {
			$green_move = $x[6];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[8].','.$x[9];
			$_SESSION['LocalGames']['LP32'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[5];
		}
		return $green_move;
	}

	// Local Pattern 27
	function LocalPattern27 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP27';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[6] | $red_move == $x[7]) {
			$green_move = $x[5];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[5]) {
			$green_move = $x[4];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[6].','.$x[7];
			$_SESSION['LocalGames']['LP33'][] = $x[0].','.$x[1].','.$x[2].','.$x[3];
		}
		return $green_move;
	}

	// Local Pattern 28
	function LocalPattern28 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP28';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[8] | $red_move == $x[9]) {
			$green_move = $x[7];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[7]) {
			$green_move = $x[6];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[8].','.$x[9];
			$_SESSION['LocalGames']['LP34'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[5];
		}
		return $green_move;
	}

	// Local Pattern 29
	function LocalPattern29 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP29';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[7] | $red_move == $x[8] | $red_move == $x[9] | $red_move == $x[10] | $red_move == $x[11] | $red_move == $x[12] | $red_move == $x[13] | $red_move == $x[15] | $red_move == $x[16] | $red_move == $x[17] | $red_move == $x[18] | $red_move == $x[22] | $red_move == $x[23] | $red_move == $x[24] | $red_move == $x[29] | $red_move == $x[30]) {
			$green_move = $x[2];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP13'][] = $x[14].','.$x[20].','.$x[21].','.$x[27].','.$x[28];
		}
		elseif ($red_move == $x[14] | $red_move == $x[20] | $red_move == $x[21] | $red_move == $x[27] | $red_move == $x[28]) {
			$green_move = $x[19];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[25].','.$x[26];
			$_SESSION['LocalGames']['LP24'][] = $x[24].','.$x[18].','.$x[17].','.$x[13].','.$x[12].','.$x[9].','.$x[8].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[19] | $red_move == $x[25]) {
			$green_move = $x[2];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP22'][] = $x[7].','.$x[10].','.$x[11].','.$x[14].','.$x[15].','.$x[16].','.$x[20].','.$x[21].','.$x[22].','.$x[23].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[30];
		}
		elseif ($red_move == $x[26]) {
			$green_move = $x[19];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP23'][] = $x[14].','.$x[20].','.$x[21].','.$x[25].','.$x[27].','.$x[28];
			$_SESSION['LocalGames']['LP24'][] = $x[24].','.$x[18].','.$x[17].','.$x[13].','.$x[12].','.$x[9].','.$x[8].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[2]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[0].','.$x[1];
			$_SESSION['LocalGames']['LP36'][] = $x[4].','.$x[5].','.$x[6].','.$x[7].','.$x[8].','.$x[9].','.$x[10].','.$x[11].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[16].','.$x[17].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[23].','.$x[24].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[30];
			$_SESSION['LocalGames']['LP24'][] = $x[24].','.$x[18].','.$x[17].','.$x[13].','.$x[12].','.$x[9].','.$x[8].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		return $green_move;
	}

	// Local Pattern 30
	function LocalPattern30 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP30';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4]) {
			$green_move = $x[5];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[5]) {
			$green_move = $x[4];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP25'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4];
		}
		return $green_move;
	}

	// Local Pattern 31
	function LocalPattern31 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP31';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[7] | $red_move == $x[8]) {
			$green_move = $x[9];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[9]) {
			$green_move = $x[6];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[7].','.$x[8];
			$_SESSION['LocalGames']['LP32'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[5];
		}
		return $green_move;
	}

	// Local Pattern 32
	function LocalPattern32 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP32';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4]) {
			$green_move = $x[5];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[5]) {
			$green_move = $x[2];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[0].','.$x[1];
			$_SESSION['LocalGames']['LP2'][] = $x[3].','.$x[4];
		}
		return $green_move;
	}

	// Local Pattern 33
	function LocalPattern33 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP33';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[3]) {
			$green_move = $x[0];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[1].','.$x[2];
		}
		return $green_move;
	}

	// Local Pattern 34
	function LocalPattern34 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP34';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4]) {
			$green_move = $x[5];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[5]) {
			$green_move = $x[2];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[0].','.$x[1];
			$_SESSION['LocalGames']['LP2'][] = $x[3].','.$x[4];
		}
		return $green_move;
	}

	// Local Pattern 35
	function LocalPattern35 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP35';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[8] | $red_move == $x[9] | $red_move == $x[12] | $red_move == $x[13] | $red_move == $x[19]) {
			$green_move = $x[2];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP5'][] = $x[7].','.$x[10].','.$x[11].','.$x[14].','.$x[15].','.$x[16].','.$x[17].','.$x[18].','.$x[22].','.$x[23].','.$x[24].','.$x[25].','.$x[29].','.$x[30].','.$x[31].','.$x[32];
		}
		elseif ($red_move == $x[7] | $red_move == $x[10] | $red_move == $x[11] | $red_move == $x[16] | $red_move == $x[17] | $red_move == $x[18] | $red_move == $x[22] | $red_move == $x[23] | $red_move == $x[24] | $red_move == $x[25] | $red_move == $x[29] | $red_move == $x[30] | $red_move == $x[31] | $red_move == $x[32]) {
			$green_move = $x[21];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[14].','.$x[15];
			$_SESSION['LocalGames']['LP2'][] = $x[27].','.$x[28];
			$_SESSION['LocalGames']['LP24'][] = $x[26].','.$x[20].','.$x[19].','.$x[13].','.$x[12].','.$x[9].','.$x[8].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[20] | $red_move == $x[26]) {
			$green_move = $x[14];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP26'][] = $x[13].','.$x[9].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
			$_SESSION['LocalGames']['LP4'][] = $x[10].','.$x[15].','.$x[16].','.$x[17].','.$x[21].','.$x[22].','.$x[23].','.$x[24].','.$x[27].','.$x[28].','.$x[29].','.$x[30].','.$x[31];
		}
		elseif ($red_move == $x[27]) {
			$green_move = $x[13];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[9].','.$x[14];
			$_SESSION['LocalGames']['LP7'][] = $x[10].','.$x[15].','.$x[16].','.$x[17].','.$x[19].','.$x[20].','.$x[22].','.$x[23].','.$x[24].','.$x[26].','.$x[28].','.$x[29].','.$x[30].','.$x[31];
			$_SESSION['LocalGames']['LP27'][] = $x[8].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[28]) {
			$green_move = $x[20];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[26].','.$x[27];
			$_SESSION['LocalGames']['LP9'][] = $x[14].','.$x[15].','.$x[16].','.$x[21].','.$x[22].','.$x[23].','.$x[29].$x[30];
			$_SESSION['LocalGames']['LP28'][] = $x[13].','.$x[9].','.$x[8].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[21]) {
			$green_move = $x[14];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP26'][] = $x[13].','.$x[9].','.$x[8].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
			$_SESSION['LocalGames']['LP6'][] = $x[10].','.$x[15].','.$x[16].','.$x[17].','.$x[20].','.$x[22].','.$x[23].','.$x[24].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[30].','.$x[31];
		}
		elseif ($red_move == $x[14]) {
			$green_move = $x[15];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP29'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[5].','.$x[6].','.$x[7].','.$x[8].','.$x[9].','.$x[10].','.$x[11].','.$x[12].','.$x[13].','.$x[16].','.$x[17].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[23].','.$x[24].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[30].','.$x[31].','.$x[32];
		}
		elseif ($red_move == $x[15]) {
			$green_move = $x[14];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP26'][] = $x[13].','.$x[9].','.$x[8].','.$x[6].','.$x[5].','.$x[4].','.$x[3].','.$x[2].','.$x[1].','.$x[0];
			$_SESSION['LocalGames']['LP16'][] = $x[7].','.$x[10].','.$x[11].','.$x[16].','.$x[17].','.$x[20].','.$x[21].','.$x[22].','.$x[23].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[30];
		}
		elseif ($red_move == $x[2]) {
			$green_move = $x[3];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[0].','.$x[1];
			$_SESSION['LocalGames']['LP40'][] = $x[4].','.$x[5].','.$x[6].','.$x[7].','.$x[8].','.$x[9].','.$x[10].','.$x[11].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[16].','.$x[17].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[23].','.$x[24].','.$x[25].','.$x[26].','.$x[27].','.$x[28].','.$x[29].','.$x[30].','.$x[31].','.$x[32];
		}
		return $green_move;
	}

	// Local Pattern 36
	function LocalPattern36 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP36';
		if ($red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[7] | $red_move == $x[8] | $red_move == $x[9] | $red_move == $x[11] | $red_move == $x[12] | $red_move == $x[13] | $red_move == $x[14] | $red_move == $x[18] | $red_move == $x[19] | $red_move == $x[20] | $red_move == $x[25] | $red_move == $x[26]) {
			$green_move = $x[0];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP13'][] = $x[10].','.$x[15].','.$x[16].','.$x[17].','.$x[21].','.$x[22].','.$x[23].','.$x[24];
		}
		elseif ($red_move == $x[10] | $red_move == $x[16] | $red_move == $x[17] | $red_move == $x[23] | $red_move == $x[24]) {
			$green_move = $x[15];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[21].','.$x[22];
			$_SESSION['LocalGames']['LP31'][] = $x[20].','.$x[14].','.$x[13].','.$x[9].','.$x[8].','.$x[5].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[15] | $red_move == $x[21]) {
			$green_move = $x[0];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP22'][] = $x[3].','.$x[6].','.$x[7].','.$x[10].','.$x[11].','.$x[12].','.$x[16].','.$x[17].','.$x[19].','.$x[22].','.$x[23].','.$x[24].','.$x[25].','.$x[26];
		}
		elseif ($red_move == $x[22]) {
			$green_move = $x[15];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP23'][] = $x[10].','.$x[16].','.$x[17].','.$x[21].','.$x[23].','.$x[24];
			$_SESSION['LocalGames']['LP31'][] = $x[20].','.$x[14].','.$x[13].','.$x[9].','.$x[8].','.$x[5].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[0]) {
			$green_move = $x[4];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[1].','.$x[2];
			$_SESSION['LocalGames']['LP37'][] = $x[3].','.$x[5].','.$x[6].','.$x[7].','.$x[8].','.$x[9].','.$x[10].','.$x[11].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[16].','.$x[17].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[23].','.$x[24].','.$x[25].','.$x[26];
		}
		return $green_move;
	}

	// Local Pattern 37
	function LocalPattern37 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP37';
		if ($red_move == $x[0] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[7] | $red_move == $x[8] | $red_move == $x[9] | $red_move == $x[10] | $red_move == $x[14] | $red_move == $x[15] | $red_move == $x[16] | $red_move == $x[21] | $red_move == $x[22]) {
			$green_move = $x[1];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP13'][] = $x[6].','.$x[12].','.$x[13].','.$x[19].','.$x[20];
		}
		elseif ($red_move == $x[6] | $red_move == $x[12] | $red_move == $x[13] | $red_move == $x[19] | $red_move == $x[20]) {
			$green_move = $x[11];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[17].','.$x[18];
			$_SESSION['LocalGames']['LP32'][] = $x[16].','.$x[10].','.$x[9].','.$x[5].','.$x[4].','.$x[1];
		}
		elseif ($red_move == $x[11] | $red_move == $x[17]) {
			$green_move = $x[1];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP22'][] = $x[0].','.$x[2].','.$x[3].','.$x[6].','.$x[7].','.$x[8].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22];
		}
		elseif ($red_move == $x[18]) {
			$green_move = $x[11];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP32'][] = $x[16].','.$x[10].','.$x[9].','.$x[5].','.$x[4].','.$x[1];
			$_SESSION['LocalGames']['LP23'][] = $x[6].','.$x[12].','.$x[13].','.$x[17].','.$x[19].','.$x[20];
		}
		elseif ($red_move == $x[1]) {
			$green_move = $x[9];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[4].','.$x[5];
			$_SESSION['LocalGames']['LP38'][] = $x[0].','.$x[2].','.$x[3].','.$x[6].','.$x[7].','.$x[10].','.$x[11].','.$x[12].','.$x[13].','.$x[16].','.$x[17].','.$x[18].','.$x[19].','.$x[20];
		}
		return $green_move;
	}

	// Local Pattern 38
	function LocalPattern38 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP38';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[7] | $red_move == $x[8] | $red_move == $x[10] | $red_move == $x[11] | $red_move == $x[12] | $red_move == $x[13]) {
			$green_move = $x[9];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[9]) {
			$green_move = $x[5];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP39'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[5].','.$x[6].','.$x[7].','.$x[8].','.$x[10].','.$x[11].','.$x[12].','.$x[13];
		}
		return $green_move;
	}

	// Local Pattern 39
	function LocalPattern39 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP39';
		if ($red_move == $x[0] | $red_move == $x[1] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[6] | $red_move == $x[7] | $red_move == $x[9] | $red_move == $x[10] | $red_move == $x[11]) {
			$green_move = $x[8];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[8]) {
			$green_move = $x[5];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP26'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[5].','.$x[6].','.$x[7].','.$x[9].','.$x[10].','.$x[11];
		}
		return $green_move;
	}

	// Local Pattern 40
	function LocalPattern40 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP40';
		if ($red_move == $x[1] | $red_move == $x[2] | $red_move == $x[4] | $red_move == $x[5] | $red_move == $x[8] | $red_move == $x[9] | $red_move == $x[15]) {
			$green_move = $x[0];
			$_SESSION['LocalGames']['LP5'][] = $x[3].','.$x[6].','.$x[7].','.$x[12].','.$x[13].','.$x[14].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[25].','.$x[26].','.$x[27].','.$x[28];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[3] | $red_move == $x[6] | $red_move == $x[7] | $red_move == $x[12] | $red_move == $x[13] | $red_move == $x[14] | $red_move == $x[18] | $red_move == $x[19] | $red_move == $x[20] | $red_move == $x[21] | $red_move == $x[25] | $red_move == $x[26] | $red_move == $x[27] | $red_move == $x[28]) {
			$green_move = $x[17];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[10].','.$x[11];
			$_SESSION['LocalGames']['LP2'][] = $x[23].','.$x[24];
			$_SESSION['LocalGames']['LP31'][] = $x[22].','.$x[16].','.$x[15].','.$x[9].','.$x[5].','.$x[4].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[16] | $red_move == $x[22]) {
			$green_move = $x[10];
			$_SESSION['LocalGames']['LP32'][] = $x[9].','.$x[5].','.$x[4].','.$x[2].','.$x[1].','.$x[0];
			$_SESSION['LocalGames']['LP4'][] = $x[6].','.$x[11].','.$x[12].','.$x[13].','.$x[17].','.$x[18].','.$x[19].','.$x[20].','.$x[23].','.$x[24].','.$x[25].','.$x[26].','.$x[27];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[23]) {
			$green_move = $x[9];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[5].','.$x[10];
			$_SESSION['LocalGames']['LP7'][] = $x[6].','.$x[11].','.$x[12].','.$x[13].','.$x[15].','.$x[16].','.$x[18].','.$x[19].','.$x[20].','.$x[22].','.$x[24].','.$x[25].','.$x[26].','.$x[27];
			$_SESSION['LocalGames']['LP33'][] = $x[4].','.$x[2].','.$x[1].','.$x[0];
		}
		elseif ($red_move == $x[24]) {
			$green_move = $x[16];
			$_SESSION['LocalGames']['LP2'][] = $x[22].','.$x[23];
			$_SESSION['LocalGames']['LP9'][] = $x[10].','.$x[11].','.$x[12].','.$x[17].','.$x[18].','.$x[19].','.$x[25].','.$x[26];
			$_SESSION['LocalGames']['LP34'][] = $x[9].','.$x[5].','.$x[4].','.$x[2].','.$x[1].','.$x[0];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[17]) {
			$green_move = $x[10];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP32'][] = $x[9].','.$x[5].','.$x[4].','.$x[2].','.$x[1].','.$x[0];
			$_SESSION['LocalGames']['LP6'][] = $x[6].','.$x[11].','.$x[12].','.$x[13].','.$x[16].','.$x[18].','.$x[19].','.$x[20].','.$x[22].','.$x[23].','.$x[24].','.$x[25].','.$x[26].','.$x[27];
		}
		elseif ($red_move == $x[10]) {
			$green_move = $x[11];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP36'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[5].','.$x[6].','.$x[7].','.$x[8].','.$x[9].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[16].','.$x[17].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[23].','.$x[24].','.$x[25].','.$x[26].','.$x[27].','.$x[28];
		}
		elseif ($red_move == $x[11]) {
			$green_move = $x[10];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP32'][] = $x[9].','.$x[5].','.$x[4].','.$x[2].','.$x[1].','.$x[0];
			$_SESSION['LocalGames']['LP16'][] = $x[3].','.$x[6].','.$x[7].','.$x[12].','.$x[13].','.$x[16].','.$x[17].','.$x[18].','.$x[19].','.$x[22].','.$x[23].','.$x[24].','.$x[25].','.$x[26];
		}
		elseif ($red_move == $x[0]) {
			$green_move = $x[4];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[1].','.$x[2];
			$_SESSION['LocalGames']['LP41'][] = $x[3].','.$x[5].','.$x[6].','.$x[7].','.$x[8].','.$x[9].','.$x[10].','.$x[11].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[16].','.$x[17].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[23].','.$x[24].','.$x[25].','.$x[26].','.$x[27].','.$x[28];
		}
		return $green_move;
	}

	// Local Pattern 41
	function LocalPattern41 ($LocalGames, $red_move, $green_move, $list) {
		$x = explode (',', $list);
		$pat = 'LP41';
		if ($red_move == $x[4] | $red_move == $x[5] | $red_move == $x[11]) {
			$green_move = $x[1];
			$_SESSION['LocalGames']['LP5'][] = $x[0].','.$x[2].','.$x[3].','.$x[8].','.$x[9].','.$x[10].','.$x[14].','.$x[15].','.$x[16].','.$x[17].','.$x[21].','.$x[22].','.$x[23].','.$x[24];
			unset_pattern ($pat, $green_move, $list);
		}
		elseif ($red_move == $x[0] | $red_move == $x[2] | $red_move == $x[3] | $red_move == $x[8] | $red_move == $x[9] | $red_move == $x[10] | $red_move == $x[14] | $red_move == $x[15] | $red_move == $x[16] | $red_move == $x[17] | $red_move == $x[21] | $red_move == $x[22] | $red_move == $x[23] | $red_move == $x[24]) {
			$green_move = $x[13];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[6].','.$x[7];
			$_SESSION['LocalGames']['LP2'][] = $x[19].','.$x[20];
			$_SESSION['LocalGames']['LP32'][] = $x[18].','.$x[12].','.$x[11].','.$x[5].','.$x[4].','.$x[1];
		}
		elseif ($red_move == $x[12] | $red_move == $x[18]) {
			$green_move = $x[6];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[5].','.$x[1];
			$_SESSION['LocalGames']['LP4'][] = $x[2].','.$x[7].','.$x[8].','.$x[9].','.$x[13].','.$x[14].','.$x[15].','.$x[16].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[23];
		}
		elseif ($red_move == $x[19]) {
			$green_move = $x[5];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[1].','.$x[6];
			$_SESSION['LocalGames']['LP7'][] = $x[2].','.$x[7].','.$x[8].','.$x[9].','.$x[11].','.$x[12].','.$x[14].','.$x[15].','.$x[16].','.$x[18].','.$x[20].','.$x[21].','.$x[22].','.$x[23];
		}
		elseif ($red_move == $x[20]) {
			$green_move = $x[12];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[18].','.$x[19];
			$_SESSION['LocalGames']['LP2'][] = $x[1].','.$x[5];
			$_SESSION['LocalGames']['LP9'][] = $x[6].','.$x[7].','.$x[8].','.$x[13].','.$x[14].','.$x[15].','.$x[21].','.$x[22];
		}
		elseif ($red_move == $x[13]) {
			$green_move = $x[6];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[1].','.$x[5];
			$_SESSION['LocalGames']['LP6'][] = $x[2].','.$x[7].','.$x[8].','.$x[9].','.$x[12].','.$x[14].','.$x[15].','.$x[16].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[23];
		}
		elseif ($red_move == $x[6]) {
			$green_move = $x[7];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP37'][] = $x[0].','.$x[1].','.$x[2].','.$x[3].','.$x[4].','.$x[5].','.$x[8].','.$x[9].','.$x[10].','.$x[11].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[16].','.$x[17].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22].','.$x[23].','.$x[24];
		}
		elseif ($red_move == $x[7]) {
			$green_move = $x[6];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[1].','.$x[5];
			$_SESSION['LocalGames']['LP16'][] = $x[0].','.$x[2].','.$x[3].','.$x[8].','.$x[9].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22];
		}
		elseif ($red_move == $x[1]) {
			$green_move = $x[11];
			unset_pattern ($pat, $green_move, $list);
			$_SESSION['LocalGames']['LP2'][] = $x[4].','.$x[5];
			$_SESSION['LocalGames']['LP2'][] = $x[6].','.$x[7];
			$_SESSION['LocalGames']['LP38'][] = $x[0].','.$x[2].','.$x[3].','.$x[8].','.$x[9].','.$x[12].','.$x[13].','.$x[14].','.$x[15].','.$x[18].','.$x[19].','.$x[20].','.$x[21].','.$x[22];
		}
		return $green_move;
	}
?>
