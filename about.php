<html>
<head>
<title>Hex</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="estilo.css" type="text/css" />
</head>
<body>
<div id="centro">
<div id="cabecera"></div>
<div class="listado">
<p><a href="index.php">Start game</a></p>
</div>
<div id="cuadro2">
<h1>Hex rules</h1>
<p>Hex is a board game in which two players play in turn and try to build a connected chain of pieces across opposite sides of the board.</p>
<p>The rules of Hex game are simple:</p>
<ul>
<li> One player plays Green and the other plays Red. Green owns the Top and Bottom sides and Red owns the Left and Right sides. </li>
<li> Green player plays first. </li>
<li> Players take turns placing a piece of their color on an unoccupied hexagon. </li>
</ul>
<!--<p>Computer plays first and you never win.</p>-->
</div>

</div>
<div class="pie">This game is based on <a href="hexpaper.pdf">this paper</a>. Programed by <a href="http://www.eduardonacimiento.com">Eduardo Nacimiento Garcia</a>
<br /> Licenced under <a href="http://www.gnu.org/licenses/agpl.html">AGPL</a>, you can dowonload it <a href="http://gitorious.org/mencey/hexgame7x7">freely</a></span>
</body>
</html>

