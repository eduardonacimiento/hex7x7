<?php
	require_once ('patterns.php');

   // Inicializa el tablero
   function inicializa_tablero () {
      $tablero = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 2, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0);
      return $tablero;
   }

   function inicializa_matrix () {
      $matrix = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      return $matrix;
   }

	function inicializa_games () {
		$_SESSION['LocalGames']['LP1'][] = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49';
	}


   // Muestra el tablero
   function mostrar ($tablero, $matrix) {
      echo '<form action="index.php" method="post">';
      $cont = 0;
      for ($h = 0; $h < 8; $h++) {
         if ($h % 2 == 0) {
            echo '<div class="celda-falsa"></div>';
         }
         for ($j = 0; $j < 12; $j++) {
            if ($tablero[$cont] == 2) {
               echo '<div class="celda" style="color: lightgreen;">ⵣ</div>';
            }
            elseif ($tablero[$cont] == 3)  {
               echo '<div class="celda" style="color: red;">ⴻ</div>';
            }
            elseif ($tablero[$cont] != 0)  {
               echo '<div class="celda"><input type="radio" name="marca" id="marca_'.$cont.'" class="radio" value="'.$cont.'" onClick="this.form.submit();"/></div>';
            }

            else {
               echo '<div class="celda"></div>';
            }
            $cont++;
         }
      }
      echo '<input type="hidden" name="tablero-enviado" value="'. serialize($tablero) .'"/>';
      echo '<input type="hidden" name="enviado" value="1"/>';
//		if (!count ($_SESSION['LocalGames']) < 1) {
//	      echo '<input type="submit" class="enviar" value="Move"/>';
//		}
      echo '</form>';
   }


function actualiza ($tablero, $marca, $jugador) {
   $tablero[$marca] = $jugador;
}



function mueve ($tablero, $mov) {
	$pattern = '0';
	$LocalGames = $_SESSION['LocalGames'];
	foreach ($LocalGames as $key => $value) {
		foreach ($LocalGames[$key] as $key2 => $value2) {
			$candidatas = explode (',', $value2);
			if (in_array ($mov, $candidatas)) {
				$pattern = substr ($key, 2);
				$list = $value2;
			}
		}
	}
	if ($pattern == 0) {
		reset ($LocalGames);
		$candidatas = each ($LocalGames);
		$list = current ($candidatas[value]);
		$pattern = substr ($candidatas[key], 2);
		$all_mov = explode (',', $list);
		$mov = $all_mov[0];
		reset ($LocalGames);
	}
	$LocalPatternX = 'LocalPattern'.$pattern;
	$pos = $LocalPatternX ($LocalGames, $mov, 0, $list);
	$posT = pos_tablero ($pos);
   return $posT;
}


function pos_real ($marca) {
	$pos = 12;
	$cont = 0;
	$i = 0;
	while ($pos < 91) {
		if ($cont < 7) {
			$i++;
			if ($pos == $marca) {
				return $i;
			}
			$cont++;
			$pos++;
		}
		else {
			$cont = 0;
			$pos += 5;
		}
	}
}


function pos_tablero ($marca) {
	$pos = 12;
	$cont = 0;
	$i = 0;
	while ($pos < 91) {
		if ($cont < 7) {
			$i++;
			if ($i == $marca) {
				return $pos;
			}
			$cont++;
			$pos++;
		}
		else {
			$cont = 0;
			$pos += 5;
		}
	}
}



function to_matrix ($tablero, $matrix) {
	$pos = 12;
	$cont = 0;
	$i = 0;
	while ($pos < 91) {
		if ($cont < 7) {
			$matrix[$i] = $tablero[$pos];
			$i++;
			$cont++;
			$pos++;
		}
		else {
			$cont = 0;
			$pos += 5;
		}
	}
}



?>
